lappend ::auto_path [pwd]
package require tkxwin

set text {hello}

proc myproc {args} {
	puts $args
}

::tkxwin::registerHotkey Control-a {after 100 "::tkxwin::sendUnicode $text"}
::tkxwin::registerHotkey Control-g {
	::tkxwin::grabKey [::tkxwin::getActiveWindowId] myproc
}
::tkxwin::registerHotkey Control-u {
	::tkxwin::ungrabKey [::tkxwin::getActiveWindowId]
}

pack [label .l -text {Press Control-a key to send text to active window}]
pack [entry .e -textvariable text]
